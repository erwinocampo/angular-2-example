/// <reference path="typings/angular2/angular2.d.ts" />
import {Component, View, bootstrap, NgFor, NgIf} from 'angular2/angular2';
import {ComponentAnnotation as Component, ViewAnnotation as View}

@Component({
  selector: 'display',
  // bindings: [FriendsService]
})
@View({
  template: `
   <p>My name: {{ myName }}</p>
   <p>Friends:</p>
	  <ul>
	     <li *ng-for="#name of names">
	        {{ name }}
	     </li>
	  </ul>
	  <p *ng-if="names.length > 3">You have many friends!</p>
  `,
  directives: [NgFor, NgIf]
})

class DisplayComponent {
  myName: string;
  names: Array<string>;
  constructor() {  // previously - (forwardRef(() => FriendsService)) friendsService:FriendsService
    this.myName = "ummmmm.....Erwin";
    this.names =  ["Janice", "Eva", "Riley", "Remy", "Ariana", "Abigail"]; // friendsService.friendnames;
  }
}

class FriendsService {
  friendnames: Array<string>;
  constructor() {
    this.friendnames = ["Janice", "Eva", "Riley", "Remy", "Ariana", "Abigail"];
  }
}

bootstrap(DisplayComponent);